# Info
1. Api prefix - /api
2. Static files prefix /static/files

# Software Versions
1. Docker - 20.10.7
2. Docker-compose - 1.17.1
3. Mongo tools


# Initial setup

## Configuration
1. Create .env at root path (can use .env.example). And set variables:  
    `COMPOSE_PROJECT_NAME`: Project prefix for docker (e.g. symbrinza_dev)  
    `APP_PORT`: Server port  
    `MONGO_PORT`: Mongo port  
    `MONGO_INITDB_ROOT_USERNAME`: Mongo username  
    `MONGO_INITDB_ROOT_PASSWORD`: Mongo password  
    `ACCESS_TOKEN`: Admin token  
    `ADMIN_USERNAME`: Default user name  
    `ADMIN_PASSWORD`: Default user password  
    `CORS_ORIGINS`: JSON array with origins  
2. Start docker-compose (look Comands)
3. Import backups
4. Create users (if not exists)

# Commands
## Start development
`sudo docker-compose -f docker-compose.yml -f docker-compose.development.yml up --build`
## Start production
`sudo docker-compose -f docker-compose.yml -f docker-compose.production.yml up -d --build`


# Extra

## Creating backups
### Database
1. Start database container (automatically starts with project docker compose)
2. Run `sh scripts/createDatabaseDump.sh` from root. It will create dump archive at backups folder 
### File storage
1. Pack all files at `file_storage` volume to file_storage_{DATE}.zip (e.g file_storage_24.06.2021.zip)


## Import backups
### Database
1. Start project docker compose
2. Run `sh scripts/importDatabaseDump.sh <ARCHIVE_PATH_FROM_ROOT>`, (e.g `sh scripts/importDatabaseDump.sh backups/database_26.08.2021.zip`) from root. It will create dump archive at backups folder 
### File storage
1. Unpack all files from file_storage archive to `file_storage` volume in _data directory  

---
## Users management
### Create: POST /api/user/:token (JSON):
    {
        "username": "username", (unique)
        "password": "password",
    }
---
### Get all: GET /api/user/:token
---
### Remove all: DELETE /api/user/all/:token (JSON)
---
### Remove: DELETE /api/user/:token (JSON):
    {
        "username": "username", (unique)
    }


## Success conditions
1. Client side can connect to server, even with 500 errors
2. No CORS and CSP errors
3. User can login to admin panel
