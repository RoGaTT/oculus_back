# UserController
``{/api/user/:token, POST}`` Create user  
``{/api/user/:token, GET}`` Get all users  
``{/api/user/:token, DELETE}`` Remove user  
  
---
# QuestController
``{/api/quest, POST}`` Create quest  
``{/api/quest, GET}`` Get all quests  
``{/api/quest/:id, GET}`` Get quest by id  
``{/api/quest/:id, PATCH}`` Update quest  
``{/api/quest/:id, DELETE}`` Remove quest  
  
---
# MetadataController
``{/api/metadata, GET}`` Get all metadata  
``{/api/metadata/main_course, GET}``  Get metadata main course  
``{/api/metadata/main_course, POST}``  Set metadata main course  
``{/api/metadata/main_quest, GET}``  Get metadata main quest  
``{/api/metadata/main_quest, POST}``  Set metadata main quest  
``{/api/metadata/courses, GET}``  Get metadata courses  
``{/api/metadata/courses, POST}``  Set metadata courses  
``{/api/metadata/quests, GET}``  Get metadata quests  
``{/api/metadata/quests, POST}``  Set metadata quests  
``{/api/metadata/materials, GET}``  Get metadata materials  
``{/api/metadata/materials, POST}``  Set metadata materials  
  
---
# CourseController
``{/api/course, POST}`` Create course  
``{/api/course, GET}`` Get all courses  
``{/api/course/:id, GET}`` Get course by id  
``{/api/course/:id, PATCH}`` Update course  
``{/api/course/:id, DELETE}`` Remove course  
  
---
# LectureController
``{/api/lecture, POST}`` Create lecture  
``{/api/lecture, GET}`` Get all lectures  
``{/api/lecture/:id, GET}`` Get lecture by id  
``{/api/lecture/:id, PATCH}`` Update lecture  
``{/api/lecture/:id, DELETE}`` Remove lecture  
  
---
# AuthorController
``{/api/author, POST}`` Create author  
``{/api/author, GET}`` Get all authors  
``{/api/author/:id, GET}`` Get author by id  
``{/api/author/:id, PATCH}`` Update author  
``{/api/author/:id, DELETE}`` Remove author  
  
---
# MaterialController
``{/api/material, POST}`` Create material  
``{/api/material, GET}`` Get all materials  
``{/api/material/:id, GET}`` Get material by id  
``{/api/material/:id, PATCH}`` Update material  
``{/api/material/:id, DELETE}`` Remove material  
  
---
# AuthController
``{/api/auth/login, POST}`` Login to admin panel  