#!/bin/bash

FILE_TO_UNZIP=$1

# Define root dir
cd $(dirname $(readlink -e "$0"))
cd ..
ROOT_DIR=$(pwd)

# Set environment variables
export $(xargs < .env)

# Unpack files to backups/dump
cd $ROOT_DIR
unzip $FILE_TO_UNZIP -d ./backups

# Import data to database
cd backups
mongorestore --username=$MONGO_INITDB_ROOT_USERNAME --password=$MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase=admin --host=localhost --port=$MONGO_PORT --drop ./dump

# Remove unpacked files
rm -rf ./dump