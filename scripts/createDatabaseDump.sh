#!/bin/bash

cd $(dirname $(readlink -e "$0"))

cd ../
export $(xargs < .env)

cd ./backups

mongodump --username=$MONGO_INITDB_ROOT_USERNAME --db=symbrinza --password=$MONGO_INITDB_ROOT_PASSWORD --authenticationDatabase=admin --excludeCollection=user --host=localhost --port=$MONGO_PORT --out=dump

zip -r ./database_$(date +"%d.%m.%G").zip ./dump

rm -rf ./dump