import { SetMetadata } from '@nestjs/common';

/**
 * Puiblic decorator, allow request without auth
 */
export const IS_PUBLIC_KEY = 'isPublic';
export const Public = () => SetMetadata(IS_PUBLIC_KEY, true);
