export type AuthConfig = {
  tokenKey: string;
  sessionSecret: string;
  JWTsecret: string;
  JWTHeaderKey: string;
};
