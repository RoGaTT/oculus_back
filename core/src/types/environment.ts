export enum EnvModeEnum {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  TEST = 'test',
  DEBUG = 'debug',
}

export type EnvConfig = {
  NODE_ENV: EnvModeEnum;
  MONGO_INITDB_ROOT_USERNAME: string;
  MONGO_INITDB_ROOT_PASSWORD: string;
  ADMIN_USERNAME: string;
  ADMIN_PASSWORD: string;
  ACCESS_TOKEN: string;
  CORS_ORIGINS: string;
  APP_PORT: string;
};
