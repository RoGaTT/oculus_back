export enum CourseMonthEnum {
  JANUARY = 'в январе',
  FEBRUARY = 'в феврале',
  MARCH = 'в марте',
  APRIL = 'в апреле',
  MAY = 'в мае',
  JUNE = 'в июне',
  JULY = 'в июле',
  AUGUST = 'в августе',
  SEPTEMBER = 'в сентябре',
  OCTOBER = 'в октябре',
  NOVEMBER = 'в ноябре',
  DECEMBER = 'в декабре',
}
