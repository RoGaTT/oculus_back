import { EnvConfig } from 'src/types/environment';

const PROJECT_CONFIG = process.env as EnvConfig;

export default PROJECT_CONFIG;
