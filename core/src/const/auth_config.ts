import { AuthConfig } from 'src/types/auth';
import PROJECT_CONFIG from './project_config';

const AUTH_CONFIG: AuthConfig = {
  tokenKey: `${PROJECT_CONFIG.NODE_ENV}_bearer_token`,
  sessionSecret: '81b7b3ac-9f42-46ae-89f4-5d0b01436487',
  JWTsecret: '42b24c55-b8c5-44d5-a480-620460fbdf6c',
  JWTHeaderKey: 'x-bearer-auth-token',
};

export default AUTH_CONFIG;
