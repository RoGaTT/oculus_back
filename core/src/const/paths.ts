const PATHS_CONFIG = {
  STATIC_CLIENT: `/var/www/mongo`,
  FILE_STORAGE: '/data/file_storage',
  FILE_STORAGE_AUTHORS: '/data/file_storage/authors',
  FILE_STORAGE_LECTURES: '/data/file_storage/lectures',
  FILE_STORAGE_COURSES: '/data/file_storage/courses',
  FILE_STORAGE_QUESTS: '/data/file_storage/quests',
  FILE_STORAGE_MATERIALS: '/data/file_storage/materials',
  FILE_STORAGE_AUTHORS_FOLDER: 'authors',
  FILE_STORAGE_LECTURES_FOLDER: 'lectures',
  FILE_STORAGE_COURSES_FOLDER: 'courses',
  FILE_STORAGE_QUESTS_FOLDER: 'quests',
  FILE_STORAGE_MATERIALS_FOLDER: 'materials',
};

export default PATHS_CONFIG;
