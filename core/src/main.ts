import { NestFactory } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import PROJECT_CONFIG from './const/project_config';
import { EnvModeEnum } from './types/environment';

/**
 * Initial script
 */
async function bootstrap() {
  let app: INestApplication | undefined;
  if (PROJECT_CONFIG.NODE_ENV === EnvModeEnum.DEVELOPMENT)
    app = await NestFactory.create(AppModule, {
      logger: console,
    });
  else {
    app = await NestFactory.create(AppModule);
  }

  // app.use(morgan('tiny'));
  app.setGlobalPrefix('api');
  app.enableCors({
    origin: JSON.parse(PROJECT_CONFIG.CORS_ORIGINS),
  });
  app.use(cookieParser());
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(8080);
  console.log(`App is listening on port: ${PROJECT_CONFIG.APP_PORT}`);
}
bootstrap();
