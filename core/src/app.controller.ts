import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
/**
 * Main APP controller
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  /**
   * Test script
   * @returns {String}
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
