import { classToPlain } from 'class-transformer';

/**
 * Serialize data with type modification
 * @param data
 */
export function customSerialize<T>(data: any) {
  return (classToPlain(data) as unknown) as T;
}
