import { cwebp } from 'webp-converter';
import { getFilenameWithoutExt } from './multer';

/**
 * Compile image to .webp
 * @param file
 */
const toWebp = (file: Express.Multer.File) => {
  const fileNameWithoutPath = getFilenameWithoutExt(file.filename);
  return cwebp(
    `${file.destination}/${file.filename}`,
    `${file.destination}/${fileNameWithoutPath}.webp`,
    '-quiet -pass 10 -alpha_method 1 -alpha_filter best -m 6 -mt -lossless -q 100',
  );
};

export default toWebp;
