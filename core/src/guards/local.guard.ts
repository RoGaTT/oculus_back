import { Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

/**
 * Local guard
 */
@Injectable()
export class LocalAuthGuard extends AuthGuard('local') {}
