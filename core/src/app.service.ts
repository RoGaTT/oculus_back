import { Injectable } from '@nestjs/common';
/**
 * App service
 */
@Injectable()
export class AppService {
  /**
   * Test script
   * @returns {String}
   */
  getHello(): string {
    return 'Hello World!';
  }
}
