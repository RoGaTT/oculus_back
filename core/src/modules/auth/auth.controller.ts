import { Controller, Post, UseGuards, Res, Req } from '@nestjs/common';
import { Response } from 'express';
import AUTH_CONFIG from 'src/const/auth_config';
import { Public } from 'src/decorators/public.decorator';
import { LocalAuthGuard } from 'src/guards/local.guard';
import { AuthService } from './auth.service';

/**
 * Auth controller, for user auth
 */
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  /**
   * Login middleware
   * @returns Auth data
   */
  @Public()
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Req() request, @Res({ passthrough: true }) response: Response) {
    const data = await this.authService.login(request.user);
    response.cookie(AUTH_CONFIG.tokenKey, data.accessToken, {
      httpOnly: true,
      domain: request.headers.host.includes('localhost')
        ? 'localhost:3000'
        : request.headers.host,
      expires: new Date(Date.now() + 1000 * 60 * 60 * 24),
    });
    return data;
  }
}
