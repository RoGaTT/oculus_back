import { Injectable } from '@nestjs/common';
import { User } from 'src/modules/user/entities/user.entity';
import { UserService } from 'src/modules/user/user.service';
import { isPasswordHashMatch } from 'src/utils/crypt';
import { JwtService } from '@nestjs/jwt';

/**
 * Auth service with extra functions
 */
@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  /**
   * Returns hash if user is valid, or null if not
   * @returns
   */
  async validateUser(
    username: string,
    password: string,
  ): Promise<Omit<User, 'hash'> | null> {
    const user = await this.usersService.findByName(username);
    const isPaswordMatch = await isPasswordHashMatch(password, user.hash);

    if (user && isPaswordMatch) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const result = new User(user);
      return result;
    }
    return null;
  }

  async login(user: User) {
    const payload = { username: user.username };
    return {
      accessToken: this.jwtService.sign(payload),
    };
  }
}
