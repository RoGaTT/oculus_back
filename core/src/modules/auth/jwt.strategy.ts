import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import AUTH_CONFIG from 'src/const/auth_config';
import { User } from 'src/modules/user/entities/user.entity';

/**
 * JWT strategy
 */
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromHeader(AUTH_CONFIG.JWTHeaderKey),
      ignoreExpiration: false,
      secretOrKey: AUTH_CONFIG.JWTsecret,
    });
  }

  async validate(payload: User) {
    return { username: payload.username };
  }
}
