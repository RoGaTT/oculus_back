import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import PROJECT_CONFIG from 'src/const/project_config';
import { generateHash } from 'src/utils/crypt';
import { customSerialize } from 'src/utils/serializer';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { RemoveUserDto } from './dto/remove-user.dto';
import { User } from './entities/user.entity';

/**
 * User service
 */
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {
    this.init();
  }

  /**
   * Init users
   */
  async init() {
    if (!PROJECT_CONFIG.ADMIN_PASSWORD || !PROJECT_CONFIG.ADMIN_USERNAME)
      return;
    const user = await this.usersRepository.find({
      where: {
        username: PROJECT_CONFIG.ADMIN_USERNAME,
      },
    });

    if (!user.length) {
      await this.create({
        username: PROJECT_CONFIG.ADMIN_USERNAME,
        password: PROJECT_CONFIG.ADMIN_PASSWORD,
      });
      console.log('Admin user created!');
    }
  }

  /**
   * Create user
   */
  async create(createUserDto: CreateUserDto) {
    const user = new User();

    user.hash = await generateHash(createUserDto.password);
    user.username = createUserDto.username;

    await this.usersRepository.save(user);

    return 'OK';
  }

  /**
   * Get all users
   */
  async findAll() {
    const fetchedList = await this.usersRepository.find();

    return fetchedList
      .filter((el) => el)
      .map((el) => customSerialize<User>(el));
  }

  /**
   * Get user by id
   */
  async findOne(id: string): Promise<User | undefined> {
    const user = await this.usersRepository.findOne(id);
    return user;
  }

  /**
   * Get user by name
   */
  async findByName(username: string): Promise<User | undefined> {
    const user = await this.usersRepository.findOne({
      where: {
        username,
      },
    });
    return user;
  }

  /**
   * Remove all users except admin
   */
  async removeAll() {
    const fetchedList = await this.usersRepository.find();

    const filteredList = fetchedList
      .filter((el) => el.username !== PROJECT_CONFIG.ADMIN_USERNAME && el)
      .map((el) => customSerialize<User>(el));

    await Promise.all(
      filteredList.map((el) => this.usersRepository.delete(el._id)),
    );

    return 'OK';
  }

  /**
   * Remove user by name
   */
  async remove(removeUserDto: RemoveUserDto) {
    if (removeUserDto.username === PROJECT_CONFIG.ADMIN_USERNAME)
      return 'You cant delete admin';
    const user = await this.findByName(removeUserDto.username);

    return await this.usersRepository.delete(user._id);
  }
}
