import { PickType } from '@nestjs/mapped-types';
import { User } from '../entities/user.entity';
import { IsString } from 'class-validator';

/**
 * Create user DTO
 */
export class CreateUserDto extends PickType(User, ['username']) {
  @IsString()
  username: string;

  @IsString()
  password: string;
}
