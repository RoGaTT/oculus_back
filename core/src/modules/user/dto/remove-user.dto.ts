import { PickType } from '@nestjs/mapped-types';
import { User } from '../entities/user.entity';
import { IsString } from 'class-validator';

/**
 * Remove user DTO
 */
export class RemoveUserDto extends PickType(User, ['username']) {
  @IsString()
  username: string;
}
