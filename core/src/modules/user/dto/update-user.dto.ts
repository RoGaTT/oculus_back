import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from './create-user.dto';

/**
 * Update user DTO
 */
export class UpdateUserDto extends PartialType(CreateUserDto) {}
