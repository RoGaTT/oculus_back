import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { Exclude, Transform } from 'class-transformer';

/**
 * User entity
 */
@Entity()
export class User {
  // @Exclude()
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column({
    unique: true,
  })
  username: string;

  @Column()
  @Exclude()
  hash: string;

  constructor(partial?: Partial<User>) {
    return Object.assign(this, partial);
  }
}
