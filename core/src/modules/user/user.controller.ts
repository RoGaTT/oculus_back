import { Controller, Get, Post, Body, Param, Delete } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { Public } from 'src/decorators/public.decorator';
import { RemoveUserDto } from './dto/remove-user.dto';
import PROJECT_CONFIG from 'src/const/project_config';

/**
 * User controller
 */
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  /**
   * Create user
   */
  @Public()
  @Post(':token')
  create(@Body() createUserDto: CreateUserDto, @Param('token') token: string) {
    if (token !== PROJECT_CONFIG.ACCESS_TOKEN) return 'Invalid token';

    return this.userService.create(createUserDto);
  }

  /**
   * Get all users
   */
  @Public()
  @Get(':token')
  findAll(@Param('token') token: string) {
    if (token !== PROJECT_CONFIG.ACCESS_TOKEN) return 'Invalid token';

    return this.userService.findAll();
  }

  /**
   * Remove all users except admin
   */
  @Public()
  @Delete('/all/:token')
  removeAll(@Param('token') token: string) {
    if (token !== PROJECT_CONFIG.ACCESS_TOKEN) return 'Invalid token';

    return this.userService.removeAll();
  }

  /**
   * Remove user by name
   */
  @Public()
  @Delete(':token')
  remove(@Body() removeUserDto: RemoveUserDto, @Param('token') token: string) {
    if (token !== PROJECT_CONFIG.ACCESS_TOKEN) return 'Invalid token';

    return this.userService.remove(removeUserDto);
  }
}
