import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import PATHS_CONFIG from 'src/const/paths';
import { getFilenameWithoutExt } from 'src/utils/multer';

/**
 * Author entity
 */
@Entity()
export class Author {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  name: string;

  @Column()
  shortName: string;

  @Column()
  info: string;

  @Column()
  @Expose()
  @Transform(
    ({ obj }) => {
      const file = getFilenameWithoutExt(obj.fallbackPhoto);
      return `${PATHS_CONFIG.FILE_STORAGE_AUTHORS_FOLDER}/${file}.webp`;
    },
    {
      toPlainOnly: true,
    },
  )
  photo: string;

  @Column()
  @Transform(
    ({ value }) => {
      return `${PATHS_CONFIG.FILE_STORAGE_AUTHORS_FOLDER}/${value}`;
    },
    {
      toPlainOnly: true,
    },
  )
  fallbackPhoto: string;

  constructor(partial?: Partial<Author>) {
    return Object.assign(this, partial);
  }
}
