import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId } from 'mongodb';
import toWebp from 'src/utils/images';
import { unlinkSync } from 'fs-extra';
import { Repository } from 'typeorm';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';
import { Author } from './entities/author.entity';
import { customSerialize } from 'src/utils/serializer';
import { getFilenameWithoutExt } from 'src/utils/multer';
import PATHS_CONFIG from 'src/const/paths';

/**
 * Author service
 */
@Injectable()
export class AuthorService {
  constructor(
    @InjectRepository(Author)
    private repository: Repository<Author>,
  ) {}

  /**
   * Create author
   */
  async create(createAuthorDto: CreateAuthorDto, file: Express.Multer.File) {
    toWebp(file);
    const author = await this.repository.save(
      new Author({
        name: createAuthorDto.name || '',
        info: createAuthorDto.info || '',
        shortName: createAuthorDto.shortName || '',
        fallbackPhoto: file.filename,
      }),
    );

    const data = await this.findOne(author._id.toHexString());

    return customSerialize<Author>(data);
  }

  /**
   * Returns all authors
   */
  async findAll() {
    const list = await this.repository.find();

    return Promise.all(list.map((el) => this.findOne(el._id.toHexString())));
  }

  /**
   * Get author by id
   */
  async findOne(id: string): Promise<Author | undefined> {
    const author = await this.repository.findOne(id);
    return author ? customSerialize<Author>(author) : undefined;
  }

  /**
   * Get authors by id list
   */
  async findByIds(idList: string[]) {
    const fetchedList = await this.repository.find({
      where: {
        _id: {
          $in: idList.map((id) => new ObjectId(id)),
        },
      },
    });

    const list = [];

    idList.forEach((id) => {
      list.push(fetchedList.find((el) => el._id.toHexString() === id));
    });

    return list.filter((el) => el).map((el) => customSerialize<Author>(el));
  }

  /**
   * Update author by id
   */
  async update(
    id: string,
    updateAuthorDto: UpdateAuthorDto,
    file?: Express.Multer.File,
  ) {
    const author = await this.repository.findOne(id);

    for (const key in updateAuthorDto) {
      const value = author[key];
      if (value === 'true' || value === 'false') author[key] = value === 'true';
      else author[key] = updateAuthorDto[key];
    }

    if (file) {
      toWebp(file);
      try {
        unlinkSync(`${file.destination}/${author.fallbackPhoto}`);
        unlinkSync(
          `${file.destination}/${getFilenameWithoutExt(
            author.fallbackPhoto,
          )}.webp`,
        );
      } catch (e) {
        console.log(e);
      }
      author.fallbackPhoto = file.filename;
    }

    await this.repository.save(author);

    return customSerialize<Author>(author);
  }

  /**
   * Remove author by id
   */
  async remove(id: string) {
    const author = await this.findOne(id);

    try {
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${author.photo}`);
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${author.fallbackPhoto}`);
    } catch (e) {
      console.log(e);
    }

    return this.repository.delete(id);
  }
}
