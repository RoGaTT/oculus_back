import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { AuthorService } from './author.service';
import { CreateAuthorDto } from './dto/create-author.dto';
import { UpdateAuthorDto } from './dto/update-author.dto';

/**
 * Author controller
 */
@Controller('author')
export class AuthorController {
  constructor(private readonly authorService: AuthorService) {}

  /**
   * Create author
   */
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  create(
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
    @Body() createAuthorDto: CreateAuthorDto,
  ) {
    const photoFile = files.photo?.[0];
    return this.authorService.create(createAuthorDto, photoFile);
  }

  /**
   * Returns all authors
   */
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  findAll() {
    return this.authorService.findAll();
  }

  /**
   * Get author by id
   */
  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  findOne(@Param('id') id: string) {
    return this.authorService.findOne(id);
  }

  /**
   * Update author by id
   */
  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  async update(
    @Param('id') id: string,
    @UploadedFiles() files: Partial<Record<string, Express.Multer.File>>,
    @Body() updateAuthorDto: UpdateAuthorDto,
  ) {
    const file = files.photo?.[0];
    return this.authorService.update(id, updateAuthorDto, file);
  }

  /**
   * Remove author by id
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.authorService.remove(id);
  }
}
