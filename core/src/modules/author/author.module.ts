import { Module } from '@nestjs/common';
import { AuthorService } from './author.service';
import { AuthorController } from './author.controller';
import { MulterModule } from '@nestjs/platform-express';
import PATHS_CONFIG from 'src/const/paths';
import { Author } from './entities/author.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { generateMulterStorage } from 'src/utils/multer';

/**
 * Author moodule
 */
@Module({
  imports: [
    TypeOrmModule.forFeature([Author]),
    MulterModule.register({
      storage: generateMulterStorage(PATHS_CONFIG.FILE_STORAGE_AUTHORS),
    }),
  ],
  controllers: [AuthorController],
  providers: [AuthorService],
  exports: [AuthorService],
})
export class AuthorModule {}
