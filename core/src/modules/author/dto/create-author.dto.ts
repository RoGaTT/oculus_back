/**
 * Create author DTO
 */
export class CreateAuthorDto {
  name: string;
  info: string;
  shortName: string;
}
