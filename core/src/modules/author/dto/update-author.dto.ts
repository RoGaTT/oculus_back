import { PartialType } from '@nestjs/mapped-types';
import { CreateAuthorDto } from './create-author.dto';

/**
 * Update author DTO
 */
export class UpdateAuthorDto extends PartialType(CreateAuthorDto) {}
