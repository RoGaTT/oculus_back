import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { classToPlain } from 'class-transformer';
import { LectureService } from 'src/modules/lecture/lecture.service';
import { Repository } from 'typeorm';
import { CreateCourseDto } from './dto/create-course.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { Course, CourseAuthor, CourseLecture } from './entities/course.entity';
import { customSerialize } from 'src/utils/serializer';
import { CourseMonthEnum } from 'src/types/course';
import { AuthorService } from '../author/author.service';
import { MetadataService } from 'src/modules/metadata/metadata.service';

/**
 * Course service
 */
@Injectable()
export class CourseService {
  constructor(
    @Inject(forwardRef(() => MetadataService))
    private metadataService: MetadataService,
    @InjectRepository(Course)
    private repository: Repository<Course>,
    private lectureService: LectureService,
    private authorService: AuthorService,
  ) {}

  /**
   * Create course
   */
  async create(createCourseDto: CreateCourseDto) {
    const course = await this.repository.save(
      new Course({
        isAnounce: !!createCourseDto.isAnounce,
        description: createCourseDto.description || '',
        startMonth: createCourseDto.startMonth || CourseMonthEnum.JANUARY,
        title: createCourseDto.title || '',
        lectures: createCourseDto.lectures.map((id) => ({
          _id: id,
        })),
        authors: createCourseDto.authors.map((id) => ({
          _id: id,
        })),
      }),
    );

    const data = await this.findOne(course._id.toHexString());

    return data;
  }

  /**
   * Get all courses
   */
  async findAll() {
    const list = await this.repository.find();
    const serializedList = list.map((el) => customSerialize<Course>(el));

    const activeList = await this.metadataService.getCourses();
    const activeIdList = activeList.map((el) => (el._id as unknown) as string);

    const filteredList = serializedList.filter(
      (el) => !activeIdList.includes((el._id as unknown) as string),
    );

    return (
      await Promise.all(
        [...activeList, ...filteredList].map((el) =>
          this.findOne((el._id as unknown) as string),
        ),
      )
    ).filter((el) => el);
  }

  /**
   * Get first course
   */
  async findFirst(): Promise<Course | undefined> {
    const list = await this.repository.find();

    return list[0] && customSerialize(list[0]);
  }

  /**
   * Get course by id
   */
  async findOne(id: string): Promise<Course | undefined> {
    const course = await this.repository.findOne(id);

    if (!course) return undefined;

    const lectureList = await this.lectureService.findByIds(
      course.lectures.map((lecture) => lecture._id),
    );
    const authorList = await this.authorService.findByIds(
      course.authors.map((author) => author._id),
    );

    course.lectures = lectureList.map((lecture, lectureIndex) => ({
      ...(course.lectures as CourseLecture[])[lectureIndex],
      data: lecture,
    }));

    course.authors = authorList.map((author, authorIndex) => ({
      ...(course.authors as CourseAuthor[])[authorIndex],
      data: author,
    }));

    return (classToPlain(course) as unknown) as Course;
  }

  /**
   * Get courses by id list
   */
  async findByIds(idList: string[]) {
    const fetchedList = await Promise.all(idList.map((id) => this.findOne(id)));

    return fetchedList
      .filter((el) => el)
      .map((el) => (classToPlain(el) as unknown) as Course);
  }

  /**
   * Update course by id
   */
  async update(id: string, updateCourseDto: UpdateCourseDto) {
    const course = await this.repository.findOne(id);

    for (const key in updateCourseDto) {
      course[key] = updateCourseDto[key];
    }

    if (updateCourseDto.lectures)
      course.lectures = updateCourseDto.lectures.map((id) => ({
        _id: id,
      }));
    if (updateCourseDto.authors)
      course.authors = updateCourseDto.authors.map((id) => ({
        _id: id,
      }));

    await this.repository.save(course);

    return await this.findOne(id);
  }

  /**
   * Remove course by id
   */
  async remove(id: string) {
    return this.repository.delete(id);
  }
}
