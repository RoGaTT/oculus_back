import { Transform } from 'class-transformer';
import { Author } from 'src/modules/author/entities/author.entity';
import { Lecture } from 'src/modules/lecture/entities/lecture.entity';
import { CourseMonthEnum } from 'src/types/course';
import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

/**
 * Course author entity
 */
@Entity()
export class CourseAuthor {
  @Column()
  _id: string;

  @Column()
  data?: Author;
}

/**
 * Course lecture entity
 */
@Entity()
export class CourseLecture {
  @Column()
  _id: string;

  @Column()
  data?: Lecture;
}

/**
 * Course entity
 */
@Entity()
export class Course {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  isAnounce: boolean;

  @Column()
  startMonth: CourseMonthEnum;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  lectures: CourseLecture[] | string[];

  @Column()
  authors: CourseAuthor[] | string[];

  constructor(partial?: Partial<Course>) {
    return Object.assign(this, partial);
  }
}
