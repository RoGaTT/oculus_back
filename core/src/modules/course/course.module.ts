import { forwardRef, Module } from '@nestjs/common';
import { CourseService } from './course.service';
import { CourseController } from './course.controller';
import { LectureModule } from '../lecture/lecture.module';
import { Course } from './entities/course.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorModule } from '../author/author.module';
import { MetadataModule } from 'src/modules/metadata/metadata.module';

/**
 * Course module
 */
@Module({
  imports: [
    LectureModule,
    AuthorModule,
    TypeOrmModule.forFeature([Course]),
    forwardRef(() => MetadataModule),
  ],
  controllers: [CourseController],
  providers: [CourseService],
  exports: [CourseService],
})
export class CourseModule {}
