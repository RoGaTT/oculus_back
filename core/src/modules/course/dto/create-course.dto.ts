import { PickType } from '@nestjs/mapped-types';
import { Course } from '../entities/course.entity';

/**
 * Create course DTO
 */
export class CreateCourseDto extends PickType(Course, [
  'title',
  'description',
  'startMonth',
]) {
  lectures: string[];
  authors: string[];
  isMain: boolean;
  isAnounce: boolean;
  isActive: boolean;
}
