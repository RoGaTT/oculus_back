import { PartialType } from '@nestjs/mapped-types';
import { CreateCourseDto } from './create-course.dto';

/**
 * Update course DTO
 */
export class UpdateCourseDto extends PartialType(CreateCourseDto) {}
