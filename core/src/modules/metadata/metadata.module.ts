import { forwardRef, Module } from '@nestjs/common';
import { MetadataService } from './metadata.service';
import { MetadataController } from './metadata.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Metadata } from './entities/metadata.entity';
import { CourseModule } from 'src/modules/course/course.module';
import { MaterialModule } from 'src/modules/material/material.module';
import { QuestModule } from 'src/modules/quest/quest.module';

/**
 * Metadata module for storing global config
 */
@Module({
  imports: [
    forwardRef(() => CourseModule),
    forwardRef(() => MaterialModule),
    forwardRef(() => QuestModule),
    TypeOrmModule.forFeature([Metadata]),
  ],
  controllers: [MetadataController],
  providers: [MetadataService],
  exports: [MetadataService],
})
export class MetadataModule {}
