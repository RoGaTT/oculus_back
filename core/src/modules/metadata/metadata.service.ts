import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CourseService } from 'src/modules/course/course.service';
import { MaterialService } from 'src/modules/material/material.service';
import { QuestService } from 'src/modules/quest/quest.service';
import { Repository } from 'typeorm';
import { Metadata } from './entities/metadata.entity';

/**
 * Metadata service
 */
@Injectable()
export class MetadataService {
  constructor(
    @Inject(forwardRef(() => CourseService))
    private courseService: CourseService,
    @Inject(forwardRef(() => QuestService))
    private questService: QuestService,
    @Inject(forwardRef(() => MaterialService))
    private materialService: MaterialService,
    @InjectRepository(Metadata)
    private repository: Repository<Metadata>,
  ) {
    this.init();
  }

  async init() {
    const list = await this.repository.find();

    if (list[0]) return list[0];

    const [mainCourse, mainQuest] = await Promise.all([
      this.questService.findFirst(),
      this.courseService.findFirst(),
    ]);

    const metadata = await this.repository.save(
      new Metadata({
        mainCourse: ((mainCourse?._id as unknown) as string) || '',
        mainQuest: ((mainQuest?._id as unknown) as string) || '',
        courses: [],
        quests: [],
        materials: [],
      }),
    );

    return metadata;
  }

  /**
   * Get all metadata
   */
  async getMetadata() {
    const list = await this.repository.find();

    if (list[0]) return list[0];
  }

  /**
   * Set main course
   */
  async setMainCourse(id: string) {
    const metadata = await this.getMetadata();

    metadata.mainCourse = id;

    return this.repository.save(metadata);
  }

  /**
   * Get main course
   */
  async getMainCourse() {
    const metadata = await this.getMetadata();

    const mainCourse = await this.courseService.findOne(metadata.mainCourse);

    return mainCourse;
  }

  /**
   * Set main quest
   */
  async setMainQuest(id: string) {
    const metadata = await this.getMetadata();

    metadata.mainQuest = id;

    return this.repository.save(metadata);
  }

  /**
   * Get main quest
   */
  async getMainQuest() {
    const metadata = await this.getMetadata();

    const mainQuest = await this.questService.findOne(metadata.mainQuest);

    return mainQuest;
  }

  /**
   * Set courses
   */
  async setCourses(idList: string[]) {
    const metadata = await this.getMetadata();

    const courseList = await this.courseService.findByIds(idList);

    courseList.sort((first, second) => {
      if (first.isAnounce && second.isAnounce) return 0;
      else if (!first.isAnounce && !second.isAnounce) return 0;
      else if (first.isAnounce) return 1;
      else return -1;
    });

    metadata.courses = courseList.map((el) => (el._id as unknown) as string);

    return this.repository.save(metadata);
  }

  /**
   * Get courses
   */
  async getCourses() {
    const metadata = await this.getMetadata();

    const courses = await this.courseService.findByIds(metadata.courses);

    return courses;
  }

  /**
   * Set quests
   */
  async setQuests(idList: string[]) {
    const metadata = await this.getMetadata();

    const list = await this.questService.findByIds(idList);

    metadata.quests = list
      .filter((el) => idList.includes((el._id as unknown) as string))
      .map((el) => (el._id as unknown) as string);

    return this.repository.save(metadata);
  }

  /**
   * Get quests
   */
  async getQuests() {
    const metadata = await this.getMetadata();

    const quests = await this.questService.findByIds(metadata.quests);

    return quests;
  }

  /**
   * Set materials
   */
  async setMaterials(idList: string[]) {
    const metadata = await this.getMetadata();

    const list = await this.materialService.findByIds(idList);

    metadata.materials = list
      .filter((el) => idList.includes((el._id as unknown) as string))
      .map((el) => (el._id as unknown) as string);

    return this.repository.save(metadata);
  }

  /**
   * Get materials
   */
  async getMaterials() {
    const metadata = await this.getMetadata();

    const materials = await this.materialService.findByIds(metadata.materials);

    return materials;
  }
}
