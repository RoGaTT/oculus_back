import { Controller, Post, Body, Get } from '@nestjs/common';
import { Public } from 'src/decorators/public.decorator';
import { MetadataService } from './metadata.service';

/**
 * Metadata controller
 */
@Controller('metadata')
export class MetadataController {
  constructor(private readonly metadataService: MetadataService) {}

  /**
   * Get all metadata
   */
  @Public()
  @Get()
  get() {
    return this.metadataService.getMetadata();
  }

  /**
   * Get main course
   */
  @Public()
  @Get('main_course')
  getMainCourse() {
    return this.metadataService.getMainCourse();
  }

  /**
   * Set main course
   */
  @Post('main_course')
  setMainCourse(@Body('id') id: string) {
    return this.metadataService.setMainCourse(id);
  }

  /**
   * Get main quest
   */
  @Public()
  @Get('main_quest')
  getMainQuest() {
    return this.metadataService.getMainQuest();
  }

  /**
   * Set main quest
   */
  @Post('main_quest')
  setMainQuest(@Body('id') id: string) {
    return this.metadataService.setMainQuest(id);
  }

  /**
   * Get courses
   */
  @Public()
  @Get('courses')
  getCourses() {
    return this.metadataService.getCourses();
  }

  /**
   * Set courses
   */
  @Post('courses')
  setCourses(@Body('idList') list: string[]) {
    return this.metadataService.setCourses(list);
  }

  /**
   * Get quests
   */
  @Public()
  @Get('quests')
  getQuests() {
    return this.metadataService.getQuests();
  }

  /**
   * Set quests
   */
  @Post('quests')
  setQuests(@Body('idList') list: string[]) {
    return this.metadataService.setQuests(list);
  }

  /**
   * Get materials
   */
  @Public()
  @Get('materials')
  getMaterials() {
    return this.metadataService.getMaterials();
  }

  /**
   * Set materials
   */
  @Post('materials')
  setMaterials(@Body('idList') list: string[]) {
    return this.metadataService.setMaterials(list);
  }
}
