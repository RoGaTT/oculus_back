import { Transform } from 'class-transformer';
import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';

/**
 * Metadata entity
 */
@Entity()
export class Metadata {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  mainCourse: string;

  @Column()
  mainQuest: string;

  @Column({
    default: [],
  })
  courses: string[];

  @Column({
    default: [],
  })
  quests: string[];

  @Column({
    default: [],
  })
  materials: string[];

  constructor(partial?: Partial<Metadata>) {
    return Object.assign(this, partial);
  }
}
