import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { MaterialService } from './material.service';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Public } from 'src/decorators/public.decorator';

/**
 * Material controller
 */
@Controller('material')
export class MaterialController {
  constructor(private readonly materialService: MaterialService) {}

  /**
   * Create material
   */
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'file', maxCount: 1 }]))
  create(
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
    @Body() createMaterialDto: CreateMaterialDto,
  ) {
    const file = files.file?.[0];
    return this.materialService.create(createMaterialDto, file);
  }

  /**
   * Get all materials
   */
  @Public()
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  findAll() {
    return this.materialService.findAll();
  }

  /**
   * Get material by id
   */
  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  findOne(@Param('id') id: string) {
    return this.materialService.findOne(id);
  }

  /**
   * Update material by id
   */
  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'file', maxCount: 1 }]))
  async update(
    @Param('id') id: string,
    @UploadedFiles() files: Partial<Record<string, Express.Multer.File>>,
    @Body() updateMaterialDto: UpdateMaterialDto,
  ) {
    const file = files.file?.[0];
    return this.materialService.update(id, updateMaterialDto, file);
  }

  /**
   * Remove material by id
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.materialService.remove(id);
  }
}
