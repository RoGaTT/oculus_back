import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { Transform } from 'class-transformer';
import PATHS_CONFIG from 'src/const/paths';

/**
 * Material entity
 */
@Entity()
export class Material {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  title: string;

  @Column()
  @Transform(
    ({ value }) => {
      return `${PATHS_CONFIG.FILE_STORAGE_MATERIALS_FOLDER}/${value}`;
    },
    {
      toPlainOnly: true,
    },
  )
  file?: string;

  @Column()
  url?: string;

  @Column()
  needVerify?: boolean;

  constructor(partial?: Partial<Material>) {
    return Object.assign(this, partial);
  }
}
