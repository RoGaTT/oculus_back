import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId } from 'mongodb';
import { unlinkSync } from 'fs-extra';
import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';
import { customSerialize } from 'src/utils/serializer';
import { MetadataService } from 'src/modules/metadata/metadata.service';
import PATHS_CONFIG from 'src/const/paths';

/**
 * Material service
 */
@Injectable()
export class MaterialService {
  constructor(
    @Inject(forwardRef(() => MetadataService))
    private metadataService: MetadataService,
    @InjectRepository(Material)
    private repository: Repository<Material>,
  ) {}

  /**
   * Create material
   */
  async create(
    createMaterialDto: CreateMaterialDto,
    file?: Express.Multer.File,
  ) {
    const material = file
      ? await this.repository.save(
          new Material({
            title: createMaterialDto.title,
            file: file.filename,
            url: createMaterialDto.url,
            needVerify: createMaterialDto.needVerify === 'true',
          }),
        )
      : await this.repository.save({
          title: createMaterialDto.title,
          url: createMaterialDto.url,
          needVerify: createMaterialDto.needVerify === 'true',
        });

    const data = await this.findOne(material._id.toHexString());

    return customSerialize<Material>(data);
  }

  /**
   * Get all materials
   */
  async findAll() {
    const list = await this.repository.find();
    const serializedList = list.map((el) => customSerialize<Material>(el));

    const activeList = await this.metadataService.getMaterials();
    const activeIdList = activeList.map((el) => (el._id as unknown) as string);

    const filteredList = serializedList.filter(
      (el) => !activeIdList.includes((el._id as unknown) as string),
    );

    return (
      await Promise.all(
        [...activeList, ...filteredList].map((el) =>
          this.findOne((el._id as unknown) as string),
        ),
      )
    ).filter((el) => el);
  }

  /**
   * Get material
   */
  async findOne(id: string): Promise<Material | undefined> {
    const material = await this.repository.findOne(id);
    return material ? customSerialize<Material>(material) : undefined;
  }

  /**
   * Get materials by is list
   */
  async findByIds(idList: string[]) {
    const fetchedList = await this.repository.find({
      where: {
        _id: {
          $in: idList.map((id) => new ObjectId(id)),
        },
      },
    });

    const list = [];

    idList.forEach((id) => {
      list.push(fetchedList.find((el) => el._id.toHexString() === id));
    });

    return list.filter((el) => el).map((el) => customSerialize<Material>(el));
  }

  /**
   * Update material by id
   */
  async update(
    id: string,
    updateMaterialDto: UpdateMaterialDto,
    file?: Express.Multer.File,
  ) {
    const material = await this.repository.findOne(id);

    for (const key in updateMaterialDto) {
      const value = updateMaterialDto[key];
      if (value === 'true' || value === 'false')
        material[key] = value === 'true';
      else material[key] = updateMaterialDto[key];
    }

    if (file) {
      try {
        unlinkSync(`${file.destination}/${material.file}`);
      } catch (e) {
        console.log(e);
      }
      material.file = file.filename;
    }

    await this.repository.save(material);

    return customSerialize<Material>(material);
  }

  /**
   * Remove material by id
   */
  async remove(id: string) {
    const material = await this.findOne(id);

    try {
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${material.file}`);
    } catch (e) {
      console.log(e);
    }

    return this.repository.delete(id);
  }
}
