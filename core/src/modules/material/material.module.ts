import { forwardRef, Module } from '@nestjs/common';
import { MaterialService } from './material.service';
import { MaterialController } from './material.controller';
import { MulterModule } from '@nestjs/platform-express';
import PATHS_CONFIG from 'src/const/paths';
import { Material } from './entities/material.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { generateMulterStorage } from 'src/utils/multer';
import { MetadataModule } from 'src/modules/metadata/metadata.module';
/**
 * Material module
 */
@Module({
  imports: [
    TypeOrmModule.forFeature([Material]),
    forwardRef(() => MetadataModule),
    MulterModule.register({
      storage: generateMulterStorage(PATHS_CONFIG.FILE_STORAGE_MATERIALS),
    }),
  ],
  controllers: [MaterialController],
  providers: [MaterialService],
  exports: [MaterialService],
})
export class MaterialModule {}
