/**
 * Create material DTO
 */
export class CreateMaterialDto {
  title: string;
  file?: string;
  url?: string;
  needVerify?: string;
}
