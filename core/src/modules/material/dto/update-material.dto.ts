import { PartialType } from '@nestjs/swagger';
import { CreateMaterialDto } from './create-material.dto';

/**
 * Update material DTO
 */
export class UpdateMaterialDto extends PartialType(CreateMaterialDto) {}
