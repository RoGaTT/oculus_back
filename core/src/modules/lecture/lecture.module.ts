import { Module } from '@nestjs/common';
import { LectureService } from './lecture.service';
import { LectureController } from './lecture.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lecture } from './entities/lecture.entity';
import { MulterModule } from '@nestjs/platform-express';
import PATHS_CONFIG from 'src/const/paths';
import { AuthorModule } from 'src/modules/author/author.module';
import { generateMulterStorage } from 'src/utils/multer';

/**
 * Lecture module
 */
@Module({
  imports: [
    TypeOrmModule.forFeature([Lecture]),
    MulterModule.register({
      storage: generateMulterStorage(PATHS_CONFIG.FILE_STORAGE_LECTURES),
    }),
    AuthorModule,
  ],
  controllers: [LectureController],
  providers: [LectureService],
  exports: [LectureService],
})
export class LectureModule {}
