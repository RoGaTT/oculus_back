import { PickType } from '@nestjs/mapped-types';
import { Lecture } from '../entities/lecture.entity';

/**
 * Create lecture DTO
 */
export class CreateLectureDto extends PickType(Lecture, ['text']) {
  fallbackPhoto?: string;
  video?: string;
}
