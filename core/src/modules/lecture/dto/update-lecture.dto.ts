import { PartialType } from '@nestjs/mapped-types';
import { CreateLectureDto } from './create-lecture.dto';

/**
 * Update lecture DTO
 */
export class UpdateLectureDto extends PartialType(CreateLectureDto) {}
