import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ClassSerializerInterceptor,
  UseInterceptors,
  UploadedFiles,
} from '@nestjs/common';
import { LectureService } from './lecture.service';
import { CreateLectureDto } from './dto/create-lecture.dto';
import { UpdateLectureDto } from './dto/update-lecture.dto';
import { FileFieldsInterceptor } from '@nestjs/platform-express';

/**
 * Lecture controller
 */
@Controller('lecture')
export class LectureController {
  constructor(private readonly lectureService: LectureService) {}

  /**
   * Create lecture
   */
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  create(@UploadedFiles() files, @Body() createAuthorDto: CreateLectureDto) {
    const photoFile = files.photo?.[0];
    return this.lectureService.create(createAuthorDto, photoFile);
  }

  /**
   * Get all lectures
   */
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  findAll() {
    return this.lectureService.findAll();
  }

  /**
   * Get lecture by id
   */
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.lectureService.findOne(id);
  }

  /**
   * Update lecture by id
   */
  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  async update(
    @Param('id') id: string,
    @UploadedFiles() files: Partial<Record<string, Express.Multer.File>>,
    @Body() updateLectureDto: UpdateLectureDto,
  ) {
    const file = files.photo?.[0];
    return this.lectureService.update(id, updateLectureDto, file);
  }

  /**
   * Remvoe lecture by id
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.lectureService.remove(id);
  }
}
