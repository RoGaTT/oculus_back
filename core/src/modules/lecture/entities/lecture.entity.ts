import { Expose, Transform } from 'class-transformer';
import PATHS_CONFIG from 'src/const/paths';
import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { getFilenameWithoutExt } from 'src/utils/multer';

/**
 * Lecture entity
 */
@Entity()
export class Lecture {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  text: string;

  @Column()
  video: string;

  @Column()
  @Expose()
  @Transform(
    ({ obj }) => {
      const file = getFilenameWithoutExt(obj.fallbackPhoto);
      return `${PATHS_CONFIG.FILE_STORAGE_AUTHORS_FOLDER}/${file}.webp`;
    },
    {
      toPlainOnly: true,
    },
  )
  photo: string;

  @Column()
  @Transform(
    ({ value }) => {
      return `${PATHS_CONFIG.FILE_STORAGE_LECTURES_FOLDER}/${value}`;
    },
    {
      toPlainOnly: true,
    },
  )
  fallbackPhoto: string;

  constructor(partial?: Partial<Lecture>) {
    return Object.assign(this, partial);
  }
}
