import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { unlinkSync } from 'fs-extra';
import { ObjectId } from 'mongodb';
import PATHS_CONFIG from 'src/const/paths';
import { AuthorService } from 'src/modules/author/author.service';
import { Author } from 'src/modules/author/entities/author.entity';
import toWebp from 'src/utils/images';
import { getFilenameWithoutExt } from 'src/utils/multer';
import { customSerialize } from 'src/utils/serializer';
import { Repository } from 'typeorm';
import { CreateLectureDto } from './dto/create-lecture.dto';
import { UpdateLectureDto } from './dto/update-lecture.dto';
import { Lecture } from './entities/lecture.entity';

/**
 * Lecture service
 */
@Injectable()
export class LectureService {
  constructor(
    @InjectRepository(Lecture)
    private repository: Repository<Lecture>,
    private authorService: AuthorService,
  ) {}

  /**
   * Create lecture
   */
  async create(createLectureDto: CreateLectureDto, file: Express.Multer.File) {
    toWebp(file);
    const lecture = await this.repository.save(
      new Lecture({
        text: createLectureDto.text || '',
        video: createLectureDto.video || '',
        fallbackPhoto: file.filename,
      }),
    );

    const data = await this.findOne(lecture._id.toHexString());

    return customSerialize<Lecture>(data);
  }

  /**
   * Get all lectures
   */
  async findAll() {
    const list = await this.repository.find();

    return Promise.all(list.map((el) => this.findOne(el._id.toHexString())));
  }

  /**
   * Get lecture by id
   */
  async findOne(id: string): Promise<Lecture | undefined> {
    const lecture = await this.repository.findOne(id);

    if (!lecture) return undefined;

    return customSerialize<Lecture>(lecture);
  }

  /**
   * Get lectures by id list
   */
  async findByIds(idList: string[]) {
    const fetchedList = await this.repository.find({
      where: {
        _id: {
          $in: idList.map((id) => new ObjectId(id)),
        },
      },
    });

    const list = [];

    idList.forEach((id) => {
      list.push(fetchedList.find((el) => el._id.toHexString() === id));
    });

    return list.filter((el) => el).map((el) => customSerialize<Lecture>(el));
  }

  /**
   * Update lecture by id
   */
  async update(
    id: string,
    updateLectureDto: UpdateLectureDto,
    file?: Express.Multer.File,
  ) {
    const lecture = await this.repository.findOne(id);

    for (const key in updateLectureDto) {
      const value = lecture[key];
      if (value === 'true' || value === 'false')
        lecture[key] = value === 'true';
      else lecture[key] = updateLectureDto[key];
    }
    if (file) {
      toWebp(file);
      try {
        unlinkSync(`${file.destination}/${lecture.fallbackPhoto}`);
        unlinkSync(
          `${file.destination}/${getFilenameWithoutExt(
            lecture.fallbackPhoto,
          )}.webp`,
        );
      } catch (e) {
        console.log(e);
      }
      lecture.fallbackPhoto = file.filename;
    }

    await this.repository.save(lecture);

    return customSerialize<Author>(lecture);
  }

  /**
   * Remove lecture by id
   */
  async remove(id: string) {
    const lecture = await this.findOne(id);

    try {
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${lecture.photo}`);
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${lecture.fallbackPhoto}`);
    } catch (e) {
      console.log(e);
    }

    return this.repository.delete(id);
  }
}
