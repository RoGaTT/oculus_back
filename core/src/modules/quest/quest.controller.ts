import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseInterceptors,
  UploadedFiles,
  ClassSerializerInterceptor,
} from '@nestjs/common';
import { FileFieldsInterceptor } from '@nestjs/platform-express';
import { QuestService } from './quest.service';
import { CreateQuestDto } from './dto/create-quest.dto';
import { UpdateQuestDto } from './dto/update-quest.dto';
import { Public } from 'src/decorators/public.decorator';

/**
 * Quest controller
 */
@Controller('quest')
export class QuestController {
  constructor(private readonly questService: QuestService) {}

  /**
   * Create quest
   */
  @Post()
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  create(
    @UploadedFiles() files: Record<string, Express.Multer.File[]>,
    @Body() createQuestDto: CreateQuestDto,
  ) {
    const photoFile = files.photo?.[0];
    return this.questService.create(createQuestDto, photoFile);
  }

  /**
   * Get all quests
   */
  @Public()
  @Get()
  @UseInterceptors(ClassSerializerInterceptor)
  findAll() {
    return this.questService.findAll();
  }

  /**
   * Get quest by id
   */
  @Get(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  findOne(@Param('id') id: string) {
    return this.questService.findOne(id);
  }

  /**
   * Update quest by id
   */
  @Patch(':id')
  @UseInterceptors(ClassSerializerInterceptor)
  @UseInterceptors(FileFieldsInterceptor([{ name: 'photo', maxCount: 1 }]))
  async update(
    @Param('id') id: string,
    @UploadedFiles() files: Partial<Record<string, Express.Multer.File>>,
    @Body() updateQuestDto: UpdateQuestDto,
  ) {
    const file = files.photo?.[0];
    return this.questService.update(id, updateQuestDto, file);
  }

  /**
   * Remove quest by id
   */
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.questService.remove(id);
  }
}
