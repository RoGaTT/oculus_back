import { forwardRef, Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId } from 'mongodb';
import toWebp from 'src/utils/images';
import { unlinkSync } from 'fs-extra';
import { Repository } from 'typeorm';
import { CreateQuestDto } from './dto/create-quest.dto';
import { UpdateQuestDto } from './dto/update-quest.dto';
import { Quest } from './entities/quest.entity';
import { customSerialize } from 'src/utils/serializer';
import { CourseMonthEnum } from 'src/types/course';
import { MetadataService } from 'src/modules/metadata/metadata.service';
import { getFilenameWithoutExt } from 'src/utils/multer';
import PATHS_CONFIG from 'src/const/paths';

/**
 * Quest service
 */
@Injectable()
export class QuestService {
  constructor(
    @Inject(forwardRef(() => MetadataService))
    private metadataService: MetadataService,
    @InjectRepository(Quest)
    private repository: Repository<Quest>,
  ) {}

  /**
   * Create quest
   */
  async create(createQuestDto: CreateQuestDto, file: Express.Multer.File) {
    toWebp(file);

    const quest = await this.repository.save(
      new Quest({
        title: createQuestDto.title,
        description: createQuestDto.description,
        isAnounce: createQuestDto.isAnounce === 'true',
        startMonth: createQuestDto.startMonth || CourseMonthEnum.JANUARY,
        fallbackPhoto: file.filename,
      }),
    );

    const data = await this.findOne(quest._id.toHexString());

    return customSerialize<Quest>(data);
  }

  /**
   * Get all quests
   */
  async findAll() {
    const list = await this.repository.find();
    const serializedList = list.map((el) => customSerialize<Quest>(el));

    const activeList = await this.metadataService.getQuests();
    const activeIdList = activeList.map((el) => (el._id as unknown) as string);

    const filteredList = serializedList.filter(
      (el) => !activeIdList.includes((el._id as unknown) as string),
    );

    return (
      await Promise.all(
        [...activeList, ...filteredList].map((el) =>
          this.findOne((el._id as unknown) as string),
        ),
      )
    ).filter((el) => el);
  }

  /**
   * Get first quest
   */
  async findFirst(): Promise<Quest | undefined> {
    const list = await this.repository.find();

    return list[0] && customSerialize(list[0]);
  }

  /**
   * Get quest by id
   */
  async findOne(id: string): Promise<Quest | undefined> {
    const quest = await this.repository.findOne(id);
    return quest ? customSerialize<Quest>(quest) : undefined;
  }

  /**
   * Get quests by id list
   */
  async findByIds(idList: string[]) {
    const fetchedList = await this.repository.find({
      where: {
        _id: {
          $in: idList.map((id) => new ObjectId(id)),
        },
      },
    });

    const list = [];

    idList.forEach((id) => {
      list.push(fetchedList.find((el) => el._id.toHexString() === id));
    });

    return list.filter((el) => el).map((el) => customSerialize<Quest>(el));
  }

  /**
   * Update quest by id
   */
  async update(
    id: string,
    updateQuestDto: UpdateQuestDto,
    file?: Express.Multer.File,
  ) {
    const quest = await this.repository.findOne(id);

    for (const key in updateQuestDto) {
      const value = updateQuestDto[key];
      if (value === 'true' || value === 'false') quest[key] = value === 'true';
      else quest[key] = updateQuestDto[key];
    }

    if (file) {
      try {
        toWebp(file);
        unlinkSync(`${file.destination}/${quest.fallbackPhoto}`);
        unlinkSync(
          `${file.destination}/${getFilenameWithoutExt(
            quest.fallbackPhoto,
          )}.webp`,
        );
        quest.fallbackPhoto = file.filename;
      } catch (e) {
        console.log(e);
      }
    }

    await this.repository.save(quest);

    return customSerialize<Quest>(quest);
  }

  /**
   * Remove quest by id
   */
  async remove(id: string) {
    const quest = await this.findOne(id);

    try {
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${quest.photo}`);
      unlinkSync(`${PATHS_CONFIG.FILE_STORAGE}/${quest.fallbackPhoto}`);
    } catch (e) {
      console.log(e);
    }

    return this.repository.delete(id);
  }
}
