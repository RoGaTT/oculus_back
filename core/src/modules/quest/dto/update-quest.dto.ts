import { PartialType } from '@nestjs/mapped-types';
import { CreateQuestDto } from './create-quest.dto';

/**
 * Update quest DTO
 */
export class UpdateQuestDto extends PartialType(CreateQuestDto) {}
