import { CourseMonthEnum } from 'src/types/course';

/**
 * Create quest DTO
 */
export class CreateQuestDto {
  title: string;
  description: string;
  isAnounce: string;
  startMonth: CourseMonthEnum;
  url: string;
}
