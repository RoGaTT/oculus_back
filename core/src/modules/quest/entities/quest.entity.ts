import { Column, Entity, ObjectID, ObjectIdColumn } from 'typeorm';
import { Expose, Transform } from 'class-transformer';
import PATHS_CONFIG from 'src/const/paths';
import { getFilenameWithoutExt } from 'src/utils/multer';
import { CourseMonthEnum } from 'src/types/course';

/**
 * Quest entity
 */
@Entity()
export class Quest {
  @ObjectIdColumn({
    generated: 'uuid',
  })
  @Transform(({ value }) => value.toHexString(), { toPlainOnly: true })
  _id: ObjectID;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  isAnounce: boolean;

  @Column()
  startMonth: CourseMonthEnum;

  @Column()
  @Expose()
  @Transform(
    ({ obj }) => {
      const file = getFilenameWithoutExt(obj.fallbackPhoto);
      return `${PATHS_CONFIG.FILE_STORAGE_QUESTS_FOLDER}/${file}.webp`;
    },
    {
      toPlainOnly: true,
    },
  )
  photo: string;

  @Column()
  @Transform(
    ({ value }) => {
      return `${PATHS_CONFIG.FILE_STORAGE_QUESTS_FOLDER}/${value}`;
    },
    {
      toPlainOnly: true,
    },
  )
  fallbackPhoto: string;

  @Column()
  url: string;

  constructor(partial?: Partial<Quest>) {
    return Object.assign(this, partial);
  }
}
