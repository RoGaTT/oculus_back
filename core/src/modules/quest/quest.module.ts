import { forwardRef, Module } from '@nestjs/common';
import { QuestService } from './quest.service';
import { QuestController } from './quest.controller';
import { MulterModule } from '@nestjs/platform-express';
import PATHS_CONFIG from 'src/const/paths';
import { Quest } from './entities/quest.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { generateMulterStorage } from 'src/utils/multer';
import { MetadataModule } from 'src/modules/metadata/metadata.module';

/**
 * Quest module
 */
@Module({
  imports: [
    TypeOrmModule.forFeature([Quest]),
    MulterModule.register({
      storage: generateMulterStorage(PATHS_CONFIG.FILE_STORAGE_QUESTS),
    }),
    forwardRef(() => MetadataModule),
  ],
  controllers: [QuestController],
  providers: [QuestService],
  exports: [QuestService],
})
export class QuestModule {}
