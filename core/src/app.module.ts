import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './modules/user/user.module';
import { QuestModule } from './modules/quest/quest.module';
import { CourseModule } from './modules/course/course.module';
import { AuthModule } from './modules/auth/auth.module';
import PROJECT_CONFIG from './const/project_config';
import { EnvModeEnum } from './types/environment';
import { AuthorModule } from './modules/author/author.module';
import { LectureModule } from './modules/lecture/lecture.module';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { APP_GUARD } from '@nestjs/core';
import { ServeStaticModule } from '@nestjs/serve-static';
import { MaterialModule } from './modules/material/material.module';
import { MetadataModule } from './modules/metadata/metadata.module';
import PATHS_CONFIG from './const/paths';

/**
 * Main APP module, with global config
 */
@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: PATHS_CONFIG.FILE_STORAGE,
      serveRoot: '/static/files',
    }),
    ServeStaticModule.forRoot({
      rootPath: PATHS_CONFIG.STATIC_CLIENT,
      serveRoot: '/static/app',
    }),
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: `mongodb://${PROJECT_CONFIG.MONGO_INITDB_ROOT_USERNAME}:${PROJECT_CONFIG.MONGO_INITDB_ROOT_PASSWORD}@mongo:27017`,
      authSource: 'admin',
      database: 'symbrinza',
      synchronize: PROJECT_CONFIG.NODE_ENV !== EnvModeEnum.PRODUCTION,
      autoLoadEntities: true,
      keepConnectionAlive: true,
      autoReconnect: true,
    }),
    UserModule,
    QuestModule,
    CourseModule,
    AuthModule,
    AuthorModule,
    LectureModule,
    MaterialModule,
    MetadataModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
